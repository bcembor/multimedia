'use strict';

/**
 * @ngdoc overview
 * @name projektApp
 * @description
 * # projektApp
 *
 * Main module of the application.
 */
angular
  .module('projektApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch', 'ui.router'
  ])
  .config(function ($stateProvider, $urlRouterProvider) {

      $urlRouterProvider.otherwise("/");
      $stateProvider
          .state('about', {
              url: '/about',
              templateUrl: 'views/about.html',
              controller: 'AboutCtrl',
              controllerAs: 'about'
          })
          .state('contact', {
              url: '/contact',
              templateUrl: 'views/contact.html',
              controller: 'ContactCtrl',
              controllerAs: 'contact'
          })
          .state('career', {
              url: '/career',
              templateUrl: 'views/career.html',
              controller: 'CareerCtrl',
              controllerAs: 'career'
          })
          .state('gallery', {
              url: '/gallery',
              templateUrl: 'views/gallery.html',
              controller: 'GalleryCtrl',
              controllerAs: 'gallery'
          })
          .state('video', {
              url: '/video',
              templateUrl: 'views/video.html',
              controller: 'VideoCtrl',
              controllerAs: 'video'
          })
          .state('disclamer', {
              url: '/disclamer',
              templateUrl: 'views/disclamer.html',
              controller: 'DisclamerCtrl',
              controllerAs: 'disclamer'
          })
          .state('main', {
              url: "/",
              templateUrl: 'views/main.html',
              controller: 'MainCtrl',
              controllerAs: 'main'
          });
  });
