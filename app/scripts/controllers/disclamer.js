'use strict';

/**
 * @ngdoc function
 * @name projektApp.controller:DisclamerCtrl
 * @description
 * # DisclamerCtrl
 * Controller of the projektApp
 */
angular.module('projektApp')
  .controller('DisclamerCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
