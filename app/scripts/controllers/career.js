'use strict';

/**
 * @ngdoc function
 * @name projektApp.controller:CareerCtrl
 * @description
 * # CareerCtrl
 * Controller of the projektApp
 */
angular.module('projektApp')
  .controller('CareerCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
