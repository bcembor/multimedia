'use strict';

/**
 * @ngdoc function
 * @name projektApp.controller:GalleryCtrl
 * @description
 * # GalleryCtrl
 * Controller of the projektApp
 */
angular.module('projektApp')
  .controller('GalleryCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
