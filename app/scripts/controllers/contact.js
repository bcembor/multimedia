'use strict';

/**
 * @ngdoc function
 * @name projektApp.controller:ContactCtrl
 * @description
 * # ContactCtrl
 * Controller of the projektApp
 */
angular.module('projektApp')
  .controller('ContactCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
