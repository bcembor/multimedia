'use strict';

/**
 * @ngdoc function
 * @name projektApp.controller:VideoCtrl
 * @description
 * # VideoCtrl
 * Controller of the projektApp
 */
angular.module('projektApp')
  .controller('VideoCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
