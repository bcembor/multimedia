'use strict';

/**
 * @ngdoc function
 * @name projektApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the projektApp
 */
angular.module('projektApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
