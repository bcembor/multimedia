'use strict';

/**
 * @ngdoc function
 * @name projektApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the projektApp
 */
angular.module('projektApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
